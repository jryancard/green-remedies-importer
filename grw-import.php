<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.northstarmarketing.com
 * @since             1.0.0
 * @package           Grw_Import
 *
 * @wordpress-plugin
 * Plugin Name:       grw-import
 * Plugin URI:        http://www.northstarmarketing.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            North Star Marketing
 * Author URI:        http://www.northstarmarketing.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       grw-import
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-grw-import-activator.php
 */
function activate_Grw_Import() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-grw-import-activator.php';
	Grw_Import_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-grw-import-deactivator.php
 */
function deactivate_Grw_Import() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-grw-import-deactivator.php';
	Grw_Import_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_Grw_Import' );
register_deactivation_hook( __FILE__, 'deactivate_Grw_Import' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-grw-import.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_Grw_Import() {

	$plugin = new Grw_Import();
	$plugin->run();

}
run_Grw_Import();
