<?php
class WP_Process_File extends WP_Background_Process {
	protected $action = 'process_file';
	protected $file;

	/**
	 * task
	 *
	 * called for each item in the queue.
	 * @param  array 	$item  The queued data
	 * @return mixed       		Return false to stop processing.  Return the
	 *                              updated $item to be processed on the next
	 *                              call to continue processing.
	 */
	protected function task( $item ) {
		//error_log( 'task' );
		global $wpdb;

		if( $myTrans = get_transient( 'cancel_import' ) ) {
			delete_transient( 'running_import' );
			delete_transient( 'cancel_import' );
			return false;
		}

		$file = file( $item['filename'] );
		$this->file = $file;

		if ( $item['row'] >= count( $this->file ) ) {
			delete_transient( 'running_import' );
			return false;
		}

		$line = str_getcsv( $this->file[ $item['row'] ] );

		$phone    = preg_replace( '/[^0-9]/', '', trim( $line[0] ) );
		$email    = trim( $line[1] );
		$company  = trim( $line[2] );
		$address1 = trim( $line[3] );
		$address2 = trim( $line[4] );
		$city     = trim( $line[5] );
		$state    = trim( $line[6] );
		$zip      = trim( $line[7] );

		$wpdb->insert( 'grw_customers', [
			'phone' => $phone,
			'email' => $email,
			'company' => $company,
			'address1' => $address1,
			'address2' => $address2,
			'city' => $city,
			'state' => $state,
			'zip' => $zip,
		]);

		if( $myTrans = get_transient( 'running_import' ) ) {
			$myTrans['current'] = $myTrans['current'] + 1;
			set_transient( 'running_import', $myTrans, HOUR_IN_SECONDS );
		} else {
			set_transient( 'running_import', array( 'total' => count( $this->file ), 'current' => $item['row'] ), HOUR_IN_SECONDS );
		}

		$item['row'] = $item['row'] + 1;

		return $item;
	}

	/**
	 * Complete
	 *
	 * Override if applicable, but ensure that the below actions are
	 * performed, or, call parent::complete().
	 */
	protected function complete() {
		delete_transient( 'running_import' );
		parent::complete();
	}
}
