<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.northstarmarketing.com
 * @since      1.0.0
 *
 * @package    Grw_Import
 * @subpackage Grw_Import/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Grw_Import
 * @subpackage Grw_Import/includes
 * @author     North Star Marketing <tech@northstarmarketing.com>
 */
class Grw_Import_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
