<?php
class Example_Background_Processing {
	protected $process_file;

	public function __construct() {
		add_action( 'plugins_loaded', array( $this, 'init' ) );
		add_action( 'init', array( $this, 'process_handler' ) );
	}

	public function init() {
		require_once plugin_dir_path( __FILE__ ) . 'background-processing/classes/wp-async-request.php';
		require_once plugin_dir_path( __FILE__ ) . 'background-processing/classes/wp-background-process.php';
		require_once plugin_dir_path( __FILE__ ) . 'background-processing/class-process-file.php';
		$this->process_file   = new WP_Process_File();
	}

	public function process_handler() {
		if ( ! isset( $_POST['process'] ) || ! isset( $_POST['_wpnonce'] ) ) {
			return;
		}
		if ( ! wp_verify_nonce( $_POST['_wpnonce'], 'grw-import-action') ) {
			return;
		}
		if ( 'cancel' === $_POST['process'] ) {
			set_transient( 'cancel_import', 'true', HOUR_IN_SECONDS );
		}
		if ( 'all' === $_POST['process'] && isset( $_POST['file'] ) ) {
			$filename = $_POST[ 'file' ];
			$file = file( $filename );
			set_transient( 'running_import', array( 'total' => count( $file ), 'current' => 0, HOUR_IN_SECONDS ) );
			$this->queue_file( $filename );
		}
	}

	protected function queue_file( $filepath ) {
		$this->process_file->push_to_queue( [ 'filename' => $filepath, 'row' => 1 ] );
		$this->process_file->save()->dispatch();
		global $wpdb;
		$wpdb->query('TRUNCATE grw_customers');
	}
}
new Example_Background_Processing();
