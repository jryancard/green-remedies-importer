<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.northstarmarketing.com
 * @since      1.0.0
 *
 * @package    Grw_Import
 * @subpackage Grw_Import/admin/partials
 */

	wp_enqueue_media();
?>
<div class="wrap">
	<h1><?php _e( 'CSV Import', 'grw-import' ); ?></h1>
	<?php if( $import_transient = get_transient( 'running_import' ) ) { ?>
		<h2>Be Patient, we're syncing</h2>
		<p>Currently importing record <?php echo $import_transient['current'] . ' of ' . $import_transient['total'];?> items.  To cancel the import click the button below. The import is happening in the background. Feel free to move browse away from this page.</p>
		<form class="" action="" method="post">
			<?php wp_nonce_field( 'grw-import-action' ); ?>
			<?php if( $myTrans = get_transient( 'cancel_import' ) ) { ?>
				<button type="submit" class="button button-primary button-large" id="end_import" name="end_import" disabled>Canceling...</button>
			<?php } else { ?>
				<input type="hidden" name="process" id="process" value="cancel">
				<button type="submit" class="button button-primary button-large" id="cancel" name="cancel">Cancel Import</button>
			<?php } ?>
		</form>
    <?php } else { ?>
		<h3>Instructions</h3>
	    <ol>
	      <li>Make sure your file has been exported as a <strong>Comma Separated File (.csv)</strong> and has the same field order as the example table below.</li>
	      <li>Use the "Select File to Import" button to select the file to import.</li>
	      <li>Click the "Start Import" button to start the upload and sync.</li>
	      <li>This import runs in the background, you are free to leave this page and do a quick dance.</li>
	    </ol>
		<form method="POST" action="" name="import_form" enctype="multipart/form-data">
			<?php wp_nonce_field( 'grw-import-action' ); ?>
			<input type="hidden" name="process" id="process" value='all' />
			<table class="form-table">
				<tbody>
					<tr>
						<th scope="row"><label for="file">File to Use</label></th>
						<td><input type="text" name="file" id="file" value="" class="regular-text" /><br><button class="button button-secondary button-large" id="upload_image_button" name="upload_image_button">Select File To Import</button></td>
					</tr>
				</tbody>
			</table>
		   <p class="submit">
    		   <button type="submit" class="button button-primary button-large" id="start_import" name="start_import">Start Import</button>
		   </p>
		</form>
		<p>Example Import File Layout:</p>
	    <div class="container">
	    	<table class="example">
				<thead>
					<tr><td>Phone</td><td>Email</td><td>Company</td><td>Address Line 1</td><td>Address Line 2</td><td>City</td><td>State</td><td>Zip</td></tr>
				</thead>
				<tr>
					<td>901-353-4691</td>
					<td>ridgecrestapts@alcomgt.com</td>
					<td>Ridgecrest Apts.</td>
					<td>2881 Rangeline Rd</td>
					<td></td>
					<td>Memphis</td>
					<td>TN</td>
					<td>38127</td>
				</tr>
				<tr>
					<td>770-443-9060</td>
					<td>campbellcreekmgr@elmingtonpm.com</td>
					<td>Campbell Creek Apts.</td>
					<td>351 W Memorial Dr.</td>
					<td></td>
					<td>Dallas</td>
					<td>GA</td>
					<td>30132</td>
				</tr>
			</table>
		</div>

		<h3>Current Data</h3>
		<div class="container">
	      <table class="example">
			<thead>
				<tr><td>Phone</td><td>Email</td><td>Company</td><td>Address Line 1</td><td>Address Line 2</td><td>City</td><td>State</td><td>Zip</td></tr>
			</thead>
			<?php
			global $wpdb;
			$customers = $wpdb->get_results("SELECT * FROM `grw_customers` ORDER by `id`");
			foreach ( $customers as $customer ) {
			?>
			<tr>
				<td><?php echo $customer->phone; ?></td>
				<td><?php echo $customer->email; ?></td>
				<td><?php echo $customer->company; ?></td>
				<td><?php echo $customer->address1; ?></td>
				<td><?php echo $customer->address2; ?></td>
				<td><?php echo $customer->city; ?></td>
				<td><?php echo $customer->state; ?></td>
				<td><?php echo $customer->zip; ?></td>
			</tr>
			<?php
			}
			?>
			</table>
		</div>
		
	<?php } ?>
</div>
<br class="clear">
