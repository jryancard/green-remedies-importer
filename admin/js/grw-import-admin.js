jQuery( document ).ready( function( $ ) {
	var file_frame;
	jQuery('#upload_image_button').on('click', function( event ){
		event.preventDefault();
		if( file_frame ) {
			file_frame.open();
			return;
		}
		file_frame = wp.media({
			title: 'Select a file',
			button: {
				text: 'Use selected file',
			},
			multiple: false,
			library: {
				type: 'text/csv'
			},
		});
		file_frame.on( 'select', function() {
			var attachment = file_frame.state().get('selection').first().toJSON();
			$( '#file' ).val( attachment.url );
		});
		file_frame.open();
	});
});
