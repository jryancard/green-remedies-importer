<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.northstarmarketing.com
 * @since      1.0.0
 *
 * @package    Grw_Import
 * @subpackage Grw_Import/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Grw_Import
 * @subpackage Grw_Import/admin
 * @author     North Star Marketing <tech@northstarmarketing.com>
 */
class Grw_Import_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Grw_Import_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Grw_Import_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/grw-import-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Grw_Import_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Grw_Import_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/grw-import-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function add_options_page() {
		$this->plugin_screen_hook_suffix = add_submenu_page(
			__( 'tools.php', 'grw-import' ),
			__( 'GRW Customer Import', 'grw-import' ),
			'Import Customers',
			'manage_options',
			$this->plugin_name,
			array( $this, 'display_options_page' )
		);

	}

	public function display_options_page() {
		include_once 'partials/grw-import-admin-display.php';
	}

	public function sample_admin_notice__success() {
		if( $import_transient = get_transient( 'running_import' ) ) {
		    ?>
		    <div class="notice notice-success is-dismissible">
		        <h3>Customer Import Running</h3>
				<p>
					Processing item <?php echo $import_transient['current'] . ' of ' .  $import_transient['total'] ;?>
					<?php if ( $cancel = get_transient( 'cancel_import' ) ) { ?>
						<br>Canceling...
					<?php } ?>
				</p>
		    </div>
		    <?php
		}
	}
}
